use clap::{Parser, Subcommand};
use dialoguer::Input;
use dialoguer::{Select, theme::ColorfulTheme};
use std::fs::{File, self};
use std::path::Path;
use std::process::Command;
use std::io::{prelude::*, self};

const STATE_PATH: &str = "/home/quirinecker/.local/state/scannerctl";
const SCANNER_PATH: &str = "/home/quirinecker/.local/state/scannerctl/scanner.txt";
const POSSIBLE_FORMATS: &'static [&'static str] = &["pnm", "tiff", "png", "jpeg"];

#[derive(Parser)]
#[command(author, version, long_about = None)]
struct Arguments {
    #[command(subcommand)]
    command: Commands
}

#[derive(Subcommand)]
enum Commands {
    /// Scans the document in the scanner
    Scan {
        output_file: Option<String>
    },
    /// Selection Menu for the printers. This will save your choice in ~/.local/state/scannerctl/scanner.txt
    Select,
    /// Creates a scanner series with e.g. (img1.png, img2.png, img3.png)
    Series,
    /// Clears persisted choices
    Clear, 
}

fn main() {
    let arguments = Arguments::parse();

    match arguments.command {
        Commands::Scan{output_file} => scan(output_file),
        Commands::Select => select(),
        Commands::Clear => clear(),
        Commands::Series => series(),
    }
}

fn clear() {
    fs::remove_dir_all(STATE_PATH).expect("could not clear settings");
}

fn scan(output_optional: Option<String>) {
    let scanner = read_file().unwrap_or_else(|_| {
        select_scanner()
    });

    let output_file = output_optional.unwrap_or("./scan.png".to_string());
    let extension = output_file.split(".").last().expect("invalid output file");
    let output = Command::new("scanimage")
        .arg("-d").arg(scanner.as_str())
        .arg("--format").arg(extension)
        .output().expect("could not scan document");

    if let Ok(err) = String::from_utf8(output.stderr) {
        println!("{}", err);
    }

    let mut file = File::create(output_file).expect("could not save scanned image");
    file.write_all(&output.stdout).expect("could not save scanned image");
}

fn select() {
    select_scanner();
}

fn select_scanner() -> String {
    println!("Looking for scanners ...");
    let scanners: Vec<String> = get_scanner_list();
    let selection = Select::with_theme(&ColorfulTheme::default())
        .with_prompt("Select Scanner: ")
        .default(0)
        .items(&scanners)
        .interact()
        .expect("failed to look for scanners");

    write_to_file(scanners[selection].to_string());
    return scanners[selection].clone()
}

fn series() {
    let path: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Path (to store the seires): ")
        .validate_with(|input: &String| {
            if Path::new(input).is_dir() { Ok(()) }
            else { Err("Not a directory") }
        })
        .interact_text()
        .unwrap();

    let prefix: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Prefix (prefix for the filename e.g (scan..))")
        .interact_text()
        .unwrap();

    let format: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Format (png, jpeg, tiff)")
        .validate_with(|input: &String| {
            if POSSIBLE_FORMATS.contains(&input.as_str()) {
                Ok(())
            } else {
                Err(format!("Invalid format. Possible formats: [{}]!", POSSIBLE_FORMATS.join(",")))
            }
        })
        .interact_text()
        .unwrap();

    let mut i = 0;

    loop {
        let final_file_name: String = Input::with_theme(&ColorfulTheme::default())
            .with_prompt(format!("Iteration {}:", i))
            .with_initial_text(format!("{}{}.{}", prefix, i, format))
            .interact_text()
            .unwrap();

        i += 1;
        scan(Some(format!("{}/{}.{}", path, final_file_name, format)))
    }
}

fn get_scanner_list() -> Vec<String> {
    let output = Command::new("scanimage").arg("-L").output()
        .expect("could not list scanners").stdout;

    let utf8_output = String::from_utf8(output).expect("invalid scan");
    return utf8_output.split("\n").map(|e| {
        return e.replace("`", "").replace("device ", "");
    }).collect();
}

fn write_to_file(content: String) {
    let path = Path::new(SCANNER_PATH);

    if let Some(parent) = path.parent() {
        fs::create_dir_all(parent).expect("could not create parent directories");
    }

    let mut file = File::create(path)
        .expect("could not save scanner setting");
    file.write(content.as_bytes())
        .expect("could not save scanner settings");
}

fn read_file() -> io::Result<String> {
    fs::read_to_string(SCANNER_PATH)
}

